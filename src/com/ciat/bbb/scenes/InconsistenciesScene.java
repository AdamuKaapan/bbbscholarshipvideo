package com.ciat.bbb.scenes;

import org.lwjgl.opengl.Display;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;

import com.ciat.bbb.Main;
import com.ciat.bbb.Scene;
import com.osreboot.ridhvl.painter.painter2d.HvlPainter2D;
import com.osreboot.ridhvl.painter.painter2d.HvlTiledRect;
import com.osreboot.ridhvl.template.HvlTemplateInteg2D;

public class InconsistenciesScene extends Scene {

	int mode;

	HvlTiledRect chromeWindow, secondChromeWindow;

	float firstWindowPos;

	float timeToFirstNote = 2.0f;
	
	float timeToSecondNote = 5.0f;
	
	float timeToThirdNote = 8.0f;
	
	float timeToBigFade = 5.0f;
	
	float bigFade = 0.0f;

	public InconsistenciesScene(Main parent) {
		super(parent);

		Texture t = HvlTemplateInteg2D.getTexture(24);
		float uvLT = 0.25f, uvBR = 0.75f;

		chromeWindow = new HvlTiledRect(t, uvLT, uvBR, 0, 0, t.getImageWidth() * uvLT, t.getImageHeight() * uvLT);
		chromeWindow.setTotalWidth(Display.getWidth() / 2);
		chromeWindow.setTotalHeight(Display.getDisplayMode().getHeight());

		secondChromeWindow = new HvlTiledRect(t, uvLT, uvBR, Display.getWidth() / 2, 0, t.getImageWidth() * uvLT, t.getImageHeight() * uvLT);
		secondChromeWindow.setTotalWidth(Display.getWidth() / 2);
		secondChromeWindow.setTotalHeight(Display.getDisplayMode().getHeight());
	}

	@Override
	public void update(float delta) {
		switch (mode) {
		case 0:
			firstWindowPos -= delta / 2;
			if (firstWindowPos <= -0.5f) {
				firstWindowPos = -0.5f;
				mode++;
			}
			break;
		case 1:
			timeToFirstNote -= delta;

			if (timeToFirstNote <= 0.0f)
				mode++;
			break;
		case 2:
			timeToSecondNote -= delta;
			
			if (timeToSecondNote <= 0.0f) 
				mode++;
			break;
		case 3:
			timeToThirdNote -= delta;
			
			if (timeToThirdNote <= 0.0f)
				mode++;
			break;
		case 4:
			timeToBigFade -= delta;
			
			if (timeToBigFade <= 0.0f)
				mode++;
			break;
		case 5:
			bigFade += delta;
			
			if (bigFade >= 1.0f) {
				bigFade = 1.0f;
				mode++;
			}
			break;
		case 6:
			parent.setCurrentScene(new ContactScene(parent));
			break;
		}
	}

	@Override
	public void draw(float delta) {
		chromeWindow.setX(Display.getWidth() * firstWindowPos);
		secondChromeWindow.setX(Display.getWidth() / 2 + Display.getWidth() * firstWindowPos);
		secondChromeWindow.setTotalWidth(Display.getWidth() - (Display.getWidth() * (firstWindowPos + 0.5f)));

		chromeWindow.draw();
		Main.segoeUI12.drawString(chromeWindow.getX() + 38, 5, "Ray-Ban Official Site", Color.darkGray);
		Main.segoeUI16.drawString(chromeWindow.getX() + 120, 30, "www.ray-ban.com", Color.darkGray);
		secondChromeWindow.draw();
		Main.segoeUI12.drawString(secondChromeWindow.getX() + 38, 5, "Totally Ray-Ban...", Color.darkGray);
		Main.segoeUI16.drawString(secondChromeWindow.getX() + 120, 30, "www.rbpus.com", Color.darkGray);

		HvlPainter2D.hvlDrawQuad(chromeWindow.getX(), 60, chromeWindow.getTotalWidth(), Display.getDisplayMode().getHeight(), Color.black);
		HvlPainter2D.hvlDrawQuad(secondChromeWindow.getX(), 60, secondChromeWindow.getTotalWidth(), Display.getDisplayMode().getHeight(), Color.black);

		HvlPainter2D.hvlDrawQuadc(chromeWindow.getX() + (chromeWindow.getTotalWidth() / 2), Display.getDisplayMode().getHeight() - ((Display.getDisplayMode().getHeight() - 60) / 2), 512, 512, HvlTemplateInteg2D.getTexture(25));
		HvlPainter2D.hvlDrawQuadc(secondChromeWindow.getX() + secondChromeWindow.getTotalWidth() * 0.5f, Display.getDisplayMode().getHeight() - ((Display.getDisplayMode().getHeight() - 60) / 2), 512, 1024, HvlTemplateInteg2D.getTexture(26));
		
		if (mode >= 2) {
			HvlPainter2D.hvlDrawQuad(secondChromeWindow.getX() + (secondChromeWindow.getTotalWidth() / 2) + 128, (secondChromeWindow.getTotalHeight() / 2) - 128, 512, 512, HvlTemplateInteg2D.getTexture(27));
		}
		if (mode >= 3) {
			HvlPainter2D.hvlDrawQuad(secondChromeWindow.getX() + (secondChromeWindow.getTotalWidth() / 2) + 246, (secondChromeWindow.getTotalHeight() / 2) + 276, 512, 256, HvlTemplateInteg2D.getTexture(28));
		}
		if (mode >= 4) {
			HvlPainter2D.hvlDrawQuad(secondChromeWindow.getX() + (secondChromeWindow.getTotalWidth() / 2) - 768, (secondChromeWindow.getTotalHeight() / 2) - 512, 512, 512, HvlTemplateInteg2D.getTexture(29));
		}
		
		HvlPainter2D.hvlDrawQuad(0, 0, Display.getWidth(), Display.getDisplayMode().getHeight(), new Color(233f / 255, 234f / 255, 237f / 255, bigFade));
		HvlPainter2D.hvlDrawQuadc(Display.getWidth() / 2, Display.getDisplayMode().getHeight() / 2, 768, 768, HvlTemplateInteg2D.getTexture(0), new Color(1f, 1f, 1f, Math.max(0f, (bigFade * 2) - 1)));
	}
}
