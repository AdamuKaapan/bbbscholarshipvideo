package com.ciat.bbb.scenes;

import org.lwjgl.opengl.Display;
import org.newdawn.slick.Color;

import com.ciat.bbb.Main;
import com.ciat.bbb.Scene;
import com.osreboot.ridhvl.HvlMath;
import com.osreboot.ridhvl.painter.painter2d.HvlPainter2D;
import com.osreboot.ridhvl.template.HvlTemplateInteg2D;

public class ContactScene extends Scene {

	int mode;
	
	float size = 1.0f;
	
	float statesOpacity = 0.0f;
	
	float timeToPhone = 3.0f;
	
	float phoneO;
	
	float timeToWebsite = 2.0f;
	
	float webO;
	
	float timeToTotalOut = 6.5f;
	
	float totalOut;
	
	public ContactScene(Main parent) {
		super(parent);
	}

	@Override
	public void update(float delta) {
		switch(mode) {
		case 0:
			size -= delta;
			
			if (size <= -0.5f) {
				size = -0.5f;
				mode++;
			}
			break;
		case 1:
			statesOpacity += delta;
			
			if (statesOpacity >= 1.0f) {
				statesOpacity = 1.0f;
				mode++;
			}
			break;
		case 2:
			timeToPhone -= delta;
			
			if (timeToPhone <= 0.0f) {
				mode++;
			}
			break;
		case 3:
			phoneO += delta;
			
			if (phoneO >= 1.0f) {
				phoneO = 1.0f;
				mode++;
			}
			break;
		case 4:
			timeToWebsite -= delta;
			
			if (timeToWebsite <= 0.0f) {
				mode++;
			}
			break;
		case 5:
			webO += delta;
			
			if (webO >= 1.0f) {
				webO = 1.0f;
				mode++;
			}
			break;
		case 6:
			timeToTotalOut -= delta;
			
			if (timeToTotalOut <= 0.0f) {
				mode++;
			}
			break;
		case 7:
			totalOut += delta;
			
			if (totalOut >= 1.0f) {
				totalOut = 1.0f;
				mode++;
			}
			break;
		case 8:
			System.out.println(HvlTemplateInteg2D.getNewestInstance().getTimer().getTotalTime());
			parent.setCurrentScene(null);
			break;
		}
	}

	@Override
	public void draw(float delta) {
		HvlPainter2D.hvlDrawQuad(0, 0, Display.getWidth(), Display.getDisplayMode().getHeight(), new Color(233f / 255, 234f / 255, 237f / 255));
		
		HvlPainter2D.hvlDrawQuadc(Display.getWidth() / 2, Display.getDisplayMode().getHeight() / 2 - HvlMath.lerp(150, 0, size), HvlMath.lerp(512, 768, Math.max(0, size)), HvlMath.lerp(512, 768, Math.max(0, size)), HvlTemplateInteg2D.getTexture(0), new Color(1f, 1f, 1f));
	
		HvlPainter2D.hvlDrawQuadc(Display.getWidth() / 2, Display.getDisplayMode().getHeight() / 2 + 80, 440, 128, HvlTemplateInteg2D.getTexture(30), new Color(1f, 1f, 1f, statesOpacity));
		
		HvlPainter2D.hvlDrawQuadc(Display.getWidth() / 2, Display.getDisplayMode().getHeight() / 2 + 232, 384, 128, HvlTemplateInteg2D.getTexture(31), new Color(1f, 1f, 1f, phoneO));
		HvlPainter2D.hvlDrawQuadc(Display.getWidth() / 2, Display.getDisplayMode().getHeight() / 2 + 384, 512, 128, HvlTemplateInteg2D.getTexture(32), new Color(1f, 1f, 1f, webO));

		HvlPainter2D.hvlDrawQuad(0, 0, Display.getWidth(), Display.getDisplayMode().getHeight(), new Color(233f / 255, 234f / 255, 237f / 255, totalOut));
	}
}
