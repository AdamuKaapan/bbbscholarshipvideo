package com.ciat.bbb.scenes;

import org.lwjgl.opengl.Display;
import org.newdawn.slick.Color;

import com.ciat.bbb.Main;
import com.ciat.bbb.Scene;
import com.osreboot.ridhvl.painter.painter2d.HvlPainter2D;
import com.osreboot.ridhvl.template.HvlTemplateInteg2D;

public class InternetRawrScene extends Scene {

	int mode;

	float internetOpacity;
	
	float timeToRawr = 0.5f;
	
	float rawr = 0f;
	
	float timeToOut = 0.75f;

	public InternetRawrScene(Main parent) {
		super(parent);
	}

	@Override
	public void update(float delta) {
		switch (mode) {
		case 0:
			internetOpacity += delta;

			if (internetOpacity >= 1.0f) {
				internetOpacity = 1.0f;
				mode++;
			}
			break;
		case 1:
			timeToRawr -= delta;
			
			if (timeToRawr <= 0.0f) {
				mode++;
			}
			break;
		case 2:
			rawr += delta * 1.5f;
			
			if (rawr >= 1.0f) {
				rawr = 1.0f;
				mode++;
			}
			break;
		case 3:
			timeToOut -= delta;
			
			if (timeToOut <= 0.0f) {
				mode++;
			}
			break;
		case 4:
			internetOpacity -= delta;
			
			if (internetOpacity <= 0.0f) {
				internetOpacity = 0.0f;
				mode++;
			}
			break;
		case 5:
			parent.setCurrentScene(new AddressMessingsScene(parent));
			break;
		}
	}

	@Override
	public void draw(float delta) {
		HvlPainter2D.hvlDrawQuad(0, 0, Display.getWidth(), Display.getDisplayMode().getHeight(), new Color(233f / 255, 234f / 255, 237f / 255));
		
		HvlPainter2D.hvlDrawQuadc(Display.getWidth() / 2, Display.getDisplayMode().getHeight() / 2, 512, 512, HvlTemplateInteg2D.getTexture(21), new Color(1f, 1f, 1f, internetOpacity));
	
		float angle = -30;
		float x = (Display.getWidth() / 2) + 200;
		float y = (Display.getDisplayMode().getHeight() / 2) - 200;
		x += rawr * 64 * (float) Math.cos(Math.toRadians(angle + 90));
		y -= rawr * 64 * (float) Math.sin(Math.toRadians(angle + 90));
		
		HvlPainter2D.hvlRotate(x, y, angle);
		HvlPainter2D.hvlDrawQuadc(x, y, 256, 128, HvlTemplateInteg2D.getTexture(22), new Color(1f, 1f, 1f, (-Math.abs(2 * rawr - 1) + 1) * 0.5f));
		HvlPainter2D.hvlResetRotation();
	}
}
