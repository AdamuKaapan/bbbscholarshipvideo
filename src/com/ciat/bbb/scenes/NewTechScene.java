package com.ciat.bbb.scenes;

import org.lwjgl.opengl.Display;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;

import com.ciat.bbb.Main;
import com.ciat.bbb.Scene;
import com.osreboot.ridhvl.painter.painter2d.HvlPainter2D;
import com.osreboot.ridhvl.template.HvlTemplateInteg2D;

public class NewTechScene extends Scene {

	int mode = -1;

	float timeToCircuit = 3.5f;
	
	float circuitOpacity = 0.0f;

	float timeToFirstGuy = 3.0f;

	float firstGuyScaleTime = 0.0f;

	float timeToSecondGuy = 0.75f;

	float secondGuyScaleTime = 0.0f;

	float timeToThirdGuy = 2.0f;

	float thirdGuyScaleTime = 0.0f;

	float timeToScaleOut = 2.0f;

	float scaleOut = 1.0f;
	
	boolean hasPlayedSound;

	public NewTechScene(Main parent) {
		super(parent);
	}

	@Override
	public void update(float delta) {
		if (!hasPlayedSound) {
//			HvlTemplateInteg2D.getSound(1).playAsSoundEffect(1.0f, 1f, false);
			hasPlayedSound = true;
		}
		
		switch (mode) {
		case -1:
			timeToCircuit -= delta;
			
			if (timeToCircuit <= 0.0f) {
				mode = 0;
			}
			break;
		case 0:
			circuitOpacity += delta * 0.75f;

			if (circuitOpacity >= 1.0f) {
				mode = 1;
				circuitOpacity = 1.0f;
			}
			break;
		case 1:
			timeToFirstGuy -= delta;

			if (timeToFirstGuy <= 0) {
				mode = 2;
			}
			break;
		case 2:
			firstGuyScaleTime += delta * 8f;

			if (firstGuyScaleTime >= 1.0f) {
				mode = 3;
				firstGuyScaleTime = 1.0f;
			}
			break;
		case 3:
			timeToSecondGuy -= delta;

			if (timeToSecondGuy <= 0) {
				mode = 4;
			}
			break;
		case 4:
			secondGuyScaleTime += delta * 8f;

			if (secondGuyScaleTime >= 1.0f) {
				mode = 5;
				secondGuyScaleTime = 1.0f;
			}
			break;
		case 5:
			timeToThirdGuy -= delta;

			if (timeToThirdGuy <= 0) {
				mode = 6;
			}
			break;
		case 6:
			thirdGuyScaleTime += delta * 8f;

			if (thirdGuyScaleTime >= 1.0f) {
				mode = 7;
				thirdGuyScaleTime = 1.0f;
			}
			break;
		case 7:
			timeToScaleOut -= delta;

			if (timeToScaleOut <= 0) {
				mode = 8;
			}
			break;
		case 8:
			scaleOut -= delta * 3f;

			if (scaleOut <= 0.0f) {
				mode = 9;
				scaleOut = 0.0f;
			}
			break;
		case 9:
			parent.setCurrentScene(new AllTheScamsScene(parent));
			break;
		}
	}

	@Override
	public void draw(float delta) {
		HvlPainter2D.hvlDrawQuad(0, 0, Display.getWidth(), Display.getDisplayMode().getHeight(), new Color(233f / 255, 234f / 255, 237f / 255));

		Texture cbt = HvlTemplateInteg2D.getTexture(6);
		if (mode < 8) {
			HvlPainter2D.hvlDrawQuadc(Display.getWidth() / 2, 192, 256, 256, cbt, new Color(1.0f, 1.0f, 1.0f, circuitOpacity));

			if (mode >= 2) {
				float scale = (-1.875f * firstGuyScaleTime * firstGuyScaleTime) + (2.875f * firstGuyScaleTime);
				float size = 256f * scale;
				HvlPainter2D.hvlDrawQuadc(Display.getWidth() / 4, Display.getDisplayMode().getHeight() - 128, size, size, HvlTemplateInteg2D.getTexture(7));
			}

			if (mode >= 4) {
				float scale = (-1.875f * secondGuyScaleTime * secondGuyScaleTime) + (2.875f * secondGuyScaleTime);
				float size = 256f * scale;
				HvlPainter2D.hvlDrawQuadc(Display.getWidth() / 2, Display.getDisplayMode().getHeight() - 128, size, size, HvlTemplateInteg2D.getTexture(8));
			}

			if (mode >= 6) {
				float scale = (-1.875f * thirdGuyScaleTime * thirdGuyScaleTime) + (2.875f * thirdGuyScaleTime);
				float size = 256f * scale;
				HvlPainter2D.hvlDrawQuadc(Display.getWidth() * 3 / 4, Display.getDisplayMode().getHeight() - 128, size, size, HvlTemplateInteg2D.getTexture(9));
			}
		}
		else {
			float scale = (-1.875f * scaleOut * scaleOut) + (2.875f * scaleOut);
			
			HvlPainter2D.hvlDrawQuadc(Display.getWidth() / 2, 192, 256 * scale, 256 * scale, cbt, new Color(1.0f, 1.0f, 1.0f, circuitOpacity));

			if (mode >= 2) {
				float size = 256f * scale;
				HvlPainter2D.hvlDrawQuadc(Display.getWidth() / 4, Display.getDisplayMode().getHeight() - 128, size, size, HvlTemplateInteg2D.getTexture(7));
			}

			if (mode >= 4) {
				float size = 256f * scale;
				HvlPainter2D.hvlDrawQuadc(Display.getWidth() / 2, Display.getDisplayMode().getHeight() - 128, size, size, HvlTemplateInteg2D.getTexture(8));
			}

			if (mode >= 6) {
				float size = 256f * scale;
				HvlPainter2D.hvlDrawQuadc(Display.getWidth() * 3 / 4, Display.getDisplayMode().getHeight() - 128, size, size, HvlTemplateInteg2D.getTexture(9));
			}
		}
	}

}
