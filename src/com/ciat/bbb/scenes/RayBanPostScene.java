package com.ciat.bbb.scenes;

import org.lwjgl.opengl.Display;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;

import com.ciat.bbb.Main;
import com.ciat.bbb.Scene;
import com.osreboot.ridhvl.HvlMath;
import com.osreboot.ridhvl.painter.painter2d.HvlPainter2D;
import com.osreboot.ridhvl.particle.collection.HvlLinearPositionProvider;
import com.osreboot.ridhvl.particle.collection.HvlSimpleParticleSystem;
import com.osreboot.ridhvl.template.HvlTemplateInteg2D;

public class RayBanPostScene extends Scene {

	public RayBanPostScene(Main parent) {
		super(parent);
//		mode = 5;
		rdpLine = new HvlLinearPositionProvider(0, 0, 64, 0);
		realDealParticles = new HvlSimpleParticleSystem(Display.getWidth() / 2, (Display.getDisplayMode().getHeight() / 2) + textBlockOffset, 16, 16, rdpLine, HvlTemplateInteg2D.getTexture(5));

		realDealParticles.setLifetime(0.5f);
		realDealParticles.setStartColor(new Color(237f / 255, 125f / 255, 49f / 255, 1.0f));
		realDealParticles.setEndColor(new Color(1.0f, 1.0f, 1.0f, 0.0f));
		realDealParticles.setParticlesPerSpawn(8);
	}

	int mode;

	float timeToStamp = 2.7f;

	float timeSinceStampIn = 0.0f;
	final float stampTimeInTotal = 0.2f;
	float stampAngle = 45f;
	float stampSizeMult = 0.9f;
	float stampStartAngle = 720 + 45f, stampEndAngle = -25f;
	float stampStartSize = 0.9f, stampEndSize = 1.5f;

	float timeUntilFadeOut = 1.5f;

	float opacity = 1.0f;

	float timeToRD = 0.25f;
	
	float textBlockOffset = 350;
	float textBlockAngle = 200.0f;

	float height = Display.getDisplayMode().getHeight() + 600;
	final float stopHeight = -1550;

	final HvlSimpleParticleSystem realDealParticles;
	final HvlLinearPositionProvider rdpLine;
	final float rdpStartDiam = 475, rdpEndDiam = rdpStartDiam + 64;
	
	float timeToTotalFadeOut = 1.0f;
	float totalFadeOut = 1.0f;
	
	boolean hasPlayedSound = false;

	@Override
	public void update(float delta) {		
		if (!hasPlayedSound) {
//			HvlTemplateInteg2D.getSound(6).playAsSoundEffect(1.0f, 0.5f, false);
			hasPlayedSound = true;
		}
		
		switch (mode) {
		case 0:
			height -= 500 * delta;
			if (height <= stopHeight) {
				height = stopHeight;
				mode = 1;
			}
			break;
		case 1:
			timeToStamp -= delta;
			if (timeToStamp <= 0) {
				mode = 2;
			}
			break;
		case 2:
			timeSinceStampIn += delta;

			if (timeSinceStampIn >= stampTimeInTotal) {
				mode = 3;
				timeSinceStampIn = stampTimeInTotal;
			}
			break;
		case 3:
			timeUntilFadeOut -= delta;

			if (timeUntilFadeOut <= 0.0f) {
				mode++;
			}

			break;
		case 4:
			opacity -= delta;
			if (opacity <= 0.0f) {
				opacity = 0.0f;
				mode = 9;
			}
			break;
		case 5:
			timeToRD -= delta;
			
			if (timeToRD <= 0.0f) {
				mode++;
			}
			break;
		case 6:
			textBlockAngle += delta * 90.0f;
			if (textBlockAngle >= 360.0f)
				mode++;
			break;
		case 7:
			timeToTotalFadeOut -= delta;
			
			if (timeToTotalFadeOut <= 0f) {
				mode++;
			}
			break;
		case 8:
			totalFadeOut -= delta;
			
			if (totalFadeOut <= 0.0f) {
				totalFadeOut = 0.0f;
				mode++;
			}
			break;
		case 9:
			parent.setCurrentScene(new AllTheScamsScene(parent));
//			parent.setCurrentScene(null);
			break;
		}
	}

	@Override
	public void draw(float delta) {
		HvlPainter2D.hvlDrawQuad(0, 0, Display.getWidth(), Display.getDisplayMode().getHeight(), new Color(233f / 255, 234f / 255, 237f / 255));
		if (mode < 5) {
			Texture fbFeedT = HvlTemplateInteg2D.getTexture(1);
			HvlPainter2D.hvlDrawQuad((Display.getWidth() / 2) - (fbFeedT.getImageWidth() / 2), height, fbFeedT.getImageWidth(), fbFeedT.getImageHeight(), fbFeedT, new Color(1.0f, 1.0f, 1.0f, opacity));
		}

		if (mode >= 2 && mode < 5) {
			HvlPainter2D.hvlRotate(Display.getWidth() / 2, Display.getDisplayMode().getHeight() / 2, HvlMath.lerp(stampStartAngle, stampEndAngle, timeSinceStampIn / stampTimeInTotal));
			HvlPainter2D.hvlDrawQuadc(Display.getWidth() / 2, Display.getDisplayMode().getHeight() / 2, 512 * HvlMath.lerp(stampStartSize, stampEndSize, timeSinceStampIn / stampTimeInTotal), 256 * HvlMath.lerp(stampStartSize, stampEndSize, timeSinceStampIn / stampTimeInTotal), HvlTemplateInteg2D.getTexture(2), new Color(1.0f, 1.0f, 1.0f, opacity));
			HvlPainter2D.hvlResetRotation();
		} else if (mode >= 5) {
			Texture rdTextT = HvlTemplateInteg2D.getTexture(3);
			Texture blockT = HvlTemplateInteg2D.getTexture(4);
			HvlPainter2D.hvlDrawQuadc(Display.getWidth() / 2, Display.getDisplayMode().getHeight() / 2, rdTextT.getImageWidth() * 0.75f, rdTextT.getImageHeight() * 0.75f, rdTextT);

			HvlPainter2D.hvlRotate(Display.getWidth() / 2, (Display.getDisplayMode().getHeight() / 2) + textBlockOffset, textBlockAngle);
			HvlPainter2D.hvlDrawQuadc(Display.getWidth() / 2, (Display.getDisplayMode().getHeight() / 2) + textBlockOffset, blockT.getImageWidth(), blockT.getImageHeight(), blockT);
			HvlPainter2D.hvlResetRotation();

			realDealParticles.setSpawnOnTimer(textBlockAngle > 235 && textBlockAngle < 303);
			
			float cos = (float) Math.cos(Math.toRadians(textBlockAngle)), sin = (float) Math.sin(Math.toRadians(textBlockAngle));
			
			realDealParticles.setMinXVel(cos * 32f);
			realDealParticles.setMaxXVel(cos * 256f);
			realDealParticles.setMinYVel(-sin * 32f);
			realDealParticles.setMaxYVel(-sin * 256f);
			
			rdpLine.setStartX(cos * rdpStartDiam);
			rdpLine.setStartY(sin * rdpStartDiam);
			rdpLine.setEndX(cos * rdpEndDiam);
			rdpLine.setEndY(sin * rdpEndDiam);
			realDealParticles.draw(delta);
		}
		HvlPainter2D.hvlDrawQuad(0, 0, Display.getWidth(), Display.getDisplayMode().getHeight(), new Color(233f / 255, 234f / 255, 237f / 255, 1.0f - totalFadeOut));
	}

}
