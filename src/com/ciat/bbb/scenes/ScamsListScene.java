package com.ciat.bbb.scenes;

import org.lwjgl.opengl.Display;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;

import com.ciat.bbb.Main;
import com.ciat.bbb.Scene;
import com.osreboot.ridhvl.HvlMath;
import com.osreboot.ridhvl.painter.painter2d.HvlPainter2D;
import com.osreboot.ridhvl.template.HvlTemplateInteg2D;

public class ScamsListScene extends Scene {

	int mode;

	float pos;
	float opacity;

	float timeToType = 0.5f;
	
	final float charTime = 0.075f;
	float timeToCharacter = charTime;
	String url = "www.rbpus.com";
	int character = 0;
	
	float highlight;
	
	float timeToStamp = 0.25f;
	
	float timeSinceStampIn = 0.0f;
	final float stampTimeInTotal = 0.2f;
	float stampAngle = 45f;
	float stampSizeMult = 0.9f;
	float stampStartAngle = 720 + 45f, stampEndAngle = -25f;
	float stampStartSize = 0.9f, stampEndSize = 1.5f;
	
	float timeToFadeOut = 1.0f;
	
	boolean hasPlayedSound;

	public ScamsListScene(Main parent) {
		super(parent);
		Texture t = HvlTemplateInteg2D.getTexture(18);
		float ratio = (float) Display.getWidth() / t.getImageWidth();

		pos = -(t.getImageHeight() * ratio) + Display.getDisplayMode().getHeight();
	}

	@Override
	public void update(float delta) {
		if (!hasPlayedSound) {
//			HvlTemplateInteg2D.getSound(2).playAsSoundEffect(1.0f, 1f, false);
			hasPlayedSound = true;
		}
		
		switch (mode) {
		case 0:
			pos += delta * 384f;
			opacity = Math.min(opacity + (delta * 0.5f), 1.0f);

			if (pos >= 0.0f) {
				pos = 0.0f;
				mode++;
			}
			break;
		case 1:
			timeToType -= delta;

			if (timeToType <= 0.0f)
				mode++;
			break;
		case 2:
			timeToCharacter -= delta;
			
			if (timeToCharacter <= 0.0f) {
				timeToCharacter = charTime;
				character++;
				if (character < url.length())
					HvlTemplateInteg2D.getSound(5).playAsSoundEffect(HvlMath.randomFloatBetween(0.8f, 1.2f), 0.4f, false);
				if (character > url.length() + 3) {
					mode++;
					HvlTemplateInteg2D.getSound(5).playAsSoundEffect(HvlMath.randomFloatBetween(0.8f, 1.2f), 0.8f, false);
				}
			}
			break;
		case 3:
			highlight += delta * 3;
			
			if (highlight >= 1.0f) {
				highlight = 1.0f;
				mode++;
			}
			break;
		case 4:
			timeToStamp -= delta;
			
			if (timeToStamp <= 0.0f) {
				mode++;
			}
			break;
		case 5:
			timeSinceStampIn += delta;

			if (timeSinceStampIn >= stampTimeInTotal) {
				mode++;
				timeSinceStampIn = stampTimeInTotal;
			}
			break;
		case 6:
			timeToFadeOut -= delta;
			
			if (timeToFadeOut <= 0.0f) {
				mode++;
			}
			break;
		case 7:
			opacity -= delta;
			
			if (opacity <= 0.0f) {
				opacity = 0.0f;
				mode++;
			}
			break;
		case 8:
			parent.setCurrentScene(new InternetRawrScene(parent));
			break;
		}
	}

	@Override
	public void draw(float delta) {
		HvlPainter2D.hvlDrawQuad(0, 0, Display.getWidth(), Display.getDisplayMode().getHeight(), new Color(233f / 255, 234f / 255, 237f / 255));

		Texture t = mode >= 3 ? HvlTemplateInteg2D.getTexture(19) : HvlTemplateInteg2D.getTexture(18);
		float ratio = (float) Display.getWidth() / t.getImageWidth();
		HvlPainter2D.hvlDrawQuad(0, pos, t.getImageWidth() * ratio, t.getImageHeight() * ratio, t, new Color(1.0f, 1.0f, 1.0f, opacity));

		HvlPainter2D.hvlDrawQuad(468, 238, 128 * highlight, 24, HvlTemplateInteg2D.getTexture(23), new Color(1.0f, 1.0f, 1.0f, 0.5f));
		
		if (mode >= 2) {
			HvlPainter2D.hvlDrawQuad(730, 98, 384, 24, new Color(1f, 1f, 1f, opacity));
			Main.sansSerif16.drawString(730, 98, url.substring(0, Math.min(character, url.length())), new Color(0f, 0f, 0f, opacity));
		}
		
		if (mode >= 5) {
			HvlPainter2D.hvlRotate(Display.getWidth() / 2, Display.getDisplayMode().getHeight() / 2, HvlMath.lerp(stampStartAngle, stampEndAngle, timeSinceStampIn / stampTimeInTotal));
			HvlPainter2D.hvlDrawQuadc(Display.getWidth() / 2, Display.getDisplayMode().getHeight() / 2, 512 * HvlMath.lerp(stampStartSize, stampEndSize, timeSinceStampIn / stampTimeInTotal), 256 * HvlMath.lerp(stampStartSize, stampEndSize, timeSinceStampIn / stampTimeInTotal), HvlTemplateInteg2D.getTexture(20), new Color(1.0f, 1.0f, 1.0f, opacity));
			HvlPainter2D.hvlResetRotation();
		}
	}
}
