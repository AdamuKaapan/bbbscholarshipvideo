package com.ciat.bbb.scenes;

import org.lwjgl.opengl.Display;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;

import com.ciat.bbb.Main;
import com.ciat.bbb.Scene;
import com.osreboot.ridhvl.painter.painter2d.HvlPainter2D;
import com.osreboot.ridhvl.painter.painter2d.HvlTiledRect;
import com.osreboot.ridhvl.particle.collection.HvlRectanglePositionProvider;
import com.osreboot.ridhvl.particle.collection.HvlSimpleParticleSystem;
import com.osreboot.ridhvl.template.HvlTemplateInteg2D;

public class AddressMessingsScene extends Scene {

	int mode;

	HvlTiledRect chromeWindow;

	float timeToExpand = 1.0f;

	float expandTime;

	float timeToTypeFirst = 1.0f;

	final float nextRealCharTime = 0.1f;
	float timeToTypeNextRealChar = nextRealCharTime;
	int realCharsExposed;
	final String realAddress = "www.ray-ban.com";

	float rbLogoOpacity = 0.0f;

	float realWindowScale = 1.0f;
	HvlTiledRect secondChromeWindow;

	float timeToTypeSecond = 1.0f;

	final float nextFakeCharTime = 0.1f;
	float timeToTypeNextFakeChar = nextFakeCharTime;
	int fakeCharsExposed;
	final String fakeAddress = "www.rbpus.com";

	float fakeOpacity = 0.0f;

	HvlSimpleParticleSystem particles;

	float timeToBigFade = 1.0f;
	
	float bigFade = 1f;

	public AddressMessingsScene(Main parent) {
		super(parent);
	}

	@Override
	public void update(float delta) {
		switch (mode) {
		case 0:
			timeToExpand -= delta;
			if (timeToExpand <= 0.0f)
				mode++;
			break;
		case 1:
			expandTime += delta * 4.0f;
			if (expandTime >= 1.0f) {
				expandTime = 1.0f;
				mode++;
				Texture t = HvlTemplateInteg2D.getTexture(24);
				float uvLT = 0.25f, uvBR = 0.75f;
				chromeWindow = new HvlTiledRect(t, uvLT, uvBR, 0, 0, t.getImageWidth() * uvLT, t.getImageHeight() * uvLT);
				chromeWindow.setTotalWidth(Display.getWidth());
				chromeWindow.setTotalHeight(Display.getDisplayMode().getHeight());
			}
			break;
		case 2:
			timeToTypeFirst -= delta;
			if (timeToTypeFirst <= 0.0f)
				mode++;
			break;
		case 3:
			timeToTypeNextRealChar -= delta;
			if (timeToTypeNextRealChar <= 0.0f) {
				timeToTypeNextRealChar = nextRealCharTime;
				realCharsExposed++;
				if (realCharsExposed >= realAddress.length())
					mode++;
			}
			break;
		case 4:
			rbLogoOpacity += delta;

			if (rbLogoOpacity >= 1.0f) {
				rbLogoOpacity = 1.0f;
				mode++;
				Texture t = HvlTemplateInteg2D.getTexture(24);
				float uvLT = 0.25f, uvBR = 0.75f;
				secondChromeWindow = new HvlTiledRect(t, uvLT, uvBR, Display.getWidth(), 0, t.getImageWidth() * uvLT, t.getImageHeight() * uvLT);
				secondChromeWindow.setTotalWidth(Display.getWidth() / 2);
				secondChromeWindow.setTotalHeight(Display.getDisplayMode().getHeight());
			}
			break;
		case 5:
			realWindowScale -= delta;

			if (realWindowScale <= 0.5f) {
				realWindowScale = 0.5f;
				mode++;
			}
			break;
		case 6:
			timeToTypeSecond -= delta;
			if (timeToTypeSecond <= 0.0f)
				mode++;
			break;
		case 7:
			timeToTypeNextFakeChar -= delta;
			if (timeToTypeNextFakeChar <= 0.0f) {
				timeToTypeNextFakeChar = nextFakeCharTime;
				fakeCharsExposed++;
				if (fakeCharsExposed >= fakeAddress.length())
					mode++;
			}
			break;
		case 8:
			fakeOpacity += delta;

			if (fakeOpacity >= 1.0f) {
				fakeOpacity = 1.0f;
				mode++;
				particles = new HvlSimpleParticleSystem(Display.getWidth() / 2, 0, 512, 256, new HvlRectanglePositionProvider(0, Display.getWidth() / 2, 0, Display.getDisplayMode().getHeight()), HvlTemplateInteg2D.getTexture(2));
				particles.setParticlesPerSpawn(1);
				particles.setTimeToSpawn(0.2f);
				particles.setMaxParticles(8);
				particles.setLifetime(2.5f);
				particles.setStartColor(Color.white);
				particles.setEndColor(Color.transparent);
				particles.setMinRot(-45f);
				particles.setMaxRot(45f);
				particles.setMinScale(0.8f);
				particles.setMaxScale(1.2f);
			}
			break;
		case 9:
			if (particles.getParticles().size() >= particles.getMaxParticles()) {
				particles.setSpawnOnTimer(false);
				mode++;
			}
			break;
		case 10:
			timeToBigFade -= delta;
			if (timeToBigFade <= 0.0f)
				mode++;
			break;
		case 11:
			bigFade -= delta;
			if (bigFade <= 0.0f) {
				bigFade = 0.0f;
				mode++;
			}
			break;
		case 12:
			parent.setCurrentScene(new InconsistenciesScene(parent));
			break;
		}
	}

	@Override
	public void draw(float delta) {
		HvlPainter2D.hvlDrawQuad(0, 0, Display.getWidth(), Display.getDisplayMode().getHeight(), new Color(233f / 255, 234f / 255, 237f / 255));

		if (mode == 0 || mode == 1) {
			float scale = -(8f / 3) * expandTime + (11f / 3) * expandTime;
			HvlPainter2D.hvlDrawQuadc(Display.getWidth() / 2, Display.getDisplayMode().getHeight() / 2, Display.getWidth() * scale, Display.getDisplayMode().getHeight() * scale, HvlTemplateInteg2D.getTexture(24));
		} else {
			chromeWindow.setTotalWidth(Display.getWidth() * realWindowScale);
			chromeWindow.draw();
			if (mode >= 5) {
				secondChromeWindow.setX(Display.getWidth() * realWindowScale);
				secondChromeWindow.draw();
			}
			HvlPainter2D.hvlDrawQuad(0, 60, chromeWindow.getTotalWidth(), Display.getDisplayMode().getHeight(), new Color(0f, 0f, 0f, rbLogoOpacity));
			if (mode >= 8)
				HvlPainter2D.hvlDrawQuad(Display.getWidth() / 2, 60, secondChromeWindow.getTotalWidth(), Display.getDisplayMode().getHeight(), new Color(0f, 0f, 0f, fakeOpacity));

			Main.segoeUI16.drawString(120, 30, realAddress.substring(0, realCharsExposed), Color.darkGray);
			Main.segoeUI16.drawString(Display.getWidth() / 2 + 120, 30, fakeAddress.substring(0, fakeCharsExposed), Color.darkGray);
			if (mode >= 4)
				Main.segoeUI12.drawString(38, 5, "Ray-Ban Official Site", Color.darkGray);

			if (mode >= 8)
				Main.segoeUI12.drawString(Display.getWidth() / 2 + 38, 5, "Totally Ray-Ban...", Color.darkGray);

			HvlPainter2D.hvlDrawQuadc(chromeWindow.getTotalWidth() / 2, Display.getDisplayMode().getHeight() - ((chromeWindow.getTotalHeight() - 60) / 2), 512, 512, HvlTemplateInteg2D.getTexture(25), new Color(1f, 1f, 1f, rbLogoOpacity));

			HvlPainter2D.hvlDrawQuadc(Display.getWidth() / 2 + chromeWindow.getTotalWidth() / 2, Display.getDisplayMode().getHeight() - ((chromeWindow.getTotalHeight() - 60) / 2), 512, 1024, HvlTemplateInteg2D.getTexture(26), new Color(1f, 1f, 1f, fakeOpacity));

			if (mode >= 9)
				particles.draw(delta);
			
			if (mode >= 10) {
				HvlPainter2D.hvlRotate(Display.getWidth() / 2 + chromeWindow.getTotalWidth() / 2, Display.getDisplayMode().getHeight() - ((chromeWindow.getTotalHeight() - 60) / 2), 15);
				HvlPainter2D.hvlDrawQuadc(Display.getWidth() / 2 + chromeWindow.getTotalWidth() / 2, Display.getDisplayMode().getHeight() - ((chromeWindow.getTotalHeight() - 60) / 2), 768, 384, HvlTemplateInteg2D.getTexture(2), new Color(1f, 1f, 1f, bigFade));
				HvlPainter2D.hvlResetRotation();
			}
		}
	}
}
