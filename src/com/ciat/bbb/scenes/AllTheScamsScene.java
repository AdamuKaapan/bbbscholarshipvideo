package com.ciat.bbb.scenes;

import org.lwjgl.opengl.Display;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;

import com.ciat.bbb.Main;
import com.ciat.bbb.Scene;
import com.osreboot.ridhvl.HvlMath;
import com.osreboot.ridhvl.painter.painter2d.HvlPainter2D;
import com.osreboot.ridhvl.template.HvlTemplateInteg2D;

public class AllTheScamsScene extends Scene {

	int mode = 0;

	float timeTo1 = 2.0f;
	float timeOn1;

	float timeTo2 = 3.0f;
	float timeOn2;

	float timeTo3 = 2.0f;

	float timeOn3, timeOn4, timeOn5, timeOn6, timeOn7, timeOn8;

	float timeBBB;
	
	float camShake = 1.0f;
	float quotesOpacity = 1.0f;
	
	float whiteout = 0.25f;
	
	boolean hasPlayedSound;

	public AllTheScamsScene(Main parent) {
		super(parent);
	}

	@Override
	public void update(float delta) {
		if (!hasPlayedSound) {
//			HvlTemplateInteg2D.getSound(1).playAsSoundEffect(1.0f, 1f, false);
			hasPlayedSound = true;
		}
		
		switch (mode) {
		case 0:
			timeTo1 -= delta;
			if (timeTo1 <= 0.0f) {
				mode = 1;
			}
			break;
		case 1:
			timeOn1 += delta * 4f;

			if (timeOn1 >= 1.0f) {
				timeOn1 = 1.0f;
				mode = 2;
			}
			break;
		case 2:
			timeTo2 -= delta;
			if (timeTo2 <= 0.0f) {
				mode++;
			}
			break;
		case 3:
			timeOn2 += delta * 4f;

			if (timeOn2 >= 1.0f) {
				timeOn2 = 1.0f;
				mode++;
			}
			break;
		case 4:
			timeTo3 -= delta;
			if (timeTo3 <= 0.0f) {
				mode++;
			}
			break;
		case 5:
			timeOn3 += delta * 4f;

			if (timeOn3 >= 1.0f) {
				timeOn3 = 1.0f;
				mode++;
			}
			break;
		case 6:
			timeOn4 += delta * 4f;

			if (timeOn4 >= 1.0f) {
				timeOn4 = 1.0f;
				mode++;
			}
			break;
		case 7:
			timeOn5 += delta * 4f;

			if (timeOn5 >= 1.0f) {
				timeOn5 = 1.0f;
				mode++;
			}
			break;
		case 8:
			timeOn6 += delta * 4f;

			if (timeOn6 >= 1.0f) {
				timeOn6 = 1.0f;
				mode++;
			}
			break;
		case 9:
			timeOn7 += delta * 4f;

			if (timeOn7 >= 1.0f) {
				timeOn7 = 1.0f;
				mode++;
			}
			break;
		case 10:
			timeOn8 += delta * 4f;

			if (timeOn8 >= 1.0f) {
				timeOn8 = 1.0f;
				mode++;
			}
			break;
		case 11:
			timeBBB += delta * 4f;

			if (timeBBB >= 1.0f) {
				timeBBB = 1.0f;
				mode++;
			}
			break;
		case 12:
			camShake = Math.max(camShake - delta, 0.0f);
			
			Main.camera.setX(HvlMath.randomFloatBetween(-8f * camShake, 8f * camShake));
			Main.camera.setY(HvlMath.randomFloatBetween(-8f * camShake, 8f * camShake));
			
			quotesOpacity -= delta * 0.33f;
			
			if (quotesOpacity <= 0.0f) {
				quotesOpacity = 0.0f;
				mode++;
			}
			break;
		case 13:
			whiteout += delta;
			if (whiteout >= 0.75f) {
				mode = 14;
			}
			break;
		case 14:
			parent.setCurrentScene(new ScamsListScene(parent));
			break;
		}
	}

	@Override
	public void draw(float delta) {
		HvlPainter2D.hvlDrawQuad(0, 0, Display.getWidth(), Display.getDisplayMode().getHeight(), new Color(233f / 255, 234f / 255, 237f / 255));

		Color c = new Color(1.0f, 1.0f, 1.0f, quotesOpacity);
		
		float scale1 = (-1.875f * timeOn1 * timeOn1) + (2.875f * timeOn1);
		Texture t1 = HvlTemplateInteg2D.getTexture(10);
		HvlPainter2D.hvlRotate(256, 256, -30);
		HvlPainter2D.hvlDrawQuadc(256, 256, t1.getImageWidth() * scale1, t1.getImageHeight() * scale1, t1, c);
		HvlPainter2D.hvlResetRotation();

		float scale2 = (-1.875f * timeOn2 * timeOn2) + (2.875f * timeOn2);
		Texture t2 = HvlTemplateInteg2D.getTexture(11);
		HvlPainter2D.hvlRotate(1024, 512, 40);
		HvlPainter2D.hvlDrawQuadc(1024, 512, t2.getImageWidth() * scale2, t2.getImageHeight() * scale2, t2, c);
		HvlPainter2D.hvlResetRotation();

		float scale3 = (-1.875f * timeOn3 * timeOn3) + (2.875f * timeOn3);
		Texture t3 = HvlTemplateInteg2D.getTexture(12);
		HvlPainter2D.hvlRotate(768, 300, -10);
		HvlPainter2D.hvlDrawQuadc(768, 300, t3.getImageWidth() * scale3, t3.getImageHeight() * scale3, t3, c);
		HvlPainter2D.hvlResetRotation();

		float scale4 = (-1.875f * timeOn4 * timeOn4) + (2.875f * timeOn4);
		Texture t4 = HvlTemplateInteg2D.getTexture(13);
		HvlPainter2D.hvlRotate(489, 145, 75);
		HvlPainter2D.hvlDrawQuadc(489, 145, t4.getImageWidth() * scale4, t4.getImageHeight() * scale4, t4, c);
		HvlPainter2D.hvlResetRotation();

		float scale5 = (-1.875f * timeOn5 * timeOn5) + (2.875f * timeOn5);
		Texture t5 = HvlTemplateInteg2D.getTexture(14);
		HvlPainter2D.hvlRotate(1300, 426, 135);
		HvlPainter2D.hvlDrawQuadc(1000, 426, t5.getImageWidth() * scale5, t5.getImageHeight() * scale5, t5, c);
		HvlPainter2D.hvlResetRotation();

		float scale6 = (-1.875f * timeOn6 * timeOn6) + (2.875f * timeOn6);
		Texture t6 = HvlTemplateInteg2D.getTexture(15);
		HvlPainter2D.hvlRotate(500, 500, -75);
		HvlPainter2D.hvlDrawQuadc(384, 500, t6.getImageWidth() * scale6, t6.getImageHeight() * scale6, t6, c);
		HvlPainter2D.hvlResetRotation();

		float scale7 = (-1.875f * timeOn7 * timeOn7) + (2.875f * timeOn7);
		Texture t7 = HvlTemplateInteg2D.getTexture(16);
		HvlPainter2D.hvlRotate(1700, 128, -45);
		HvlPainter2D.hvlDrawQuadc(1024, 128, t7.getImageWidth() * scale7, t7.getImageHeight() * scale7, t7, c);
		HvlPainter2D.hvlResetRotation();

		float scale8 = (-1.875f * timeOn8 * timeOn8) + (2.875f * timeOn8);
		Texture t8 = HvlTemplateInteg2D.getTexture(17);
		HvlPainter2D.hvlRotate(768, 384, -10);
		HvlPainter2D.hvlDrawQuadc(768, 384, t8.getImageWidth() * scale8, t8.getImageHeight() * scale8, t8, c);
		HvlPainter2D.hvlResetRotation();

		if (mode >= 11) {
			float scale = ((-1.875f * timeBBB * timeBBB) + (2.875f * timeBBB)) * 0.5f;

			Texture t = HvlTemplateInteg2D.getTexture(0);
			HvlPainter2D.hvlDrawQuadc(Display.getWidth() / 2, Display.getDisplayMode().getHeight() / 2, t.getImageWidth() * scale, t.getImageHeight() * scale, t);
		}
		
		if (mode >= 13) {
			HvlPainter2D.hvlDrawQuad((whiteout * Display.getWidth()) - Display.getWidth(), 0, Display.getWidth(), Display.getDisplayMode().getHeight(), new Color(233f / 255, 234f / 255, 237f / 255));
		}
	}
}
