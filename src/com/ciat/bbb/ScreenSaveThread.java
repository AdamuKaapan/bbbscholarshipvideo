package com.ciat.bbb;

import java.nio.ByteBuffer;

public class ScreenSaveThread extends Thread {
	private ByteBuffer toSave;
	private String path;
	
	public ScreenSaveThread(ByteBuffer toSave, String path) {
		this.toSave = toSave;
		this.path = path;
	}

	@Override
	public void run() {
		Main.saveScreenshot(toSave, path);
	}
}
