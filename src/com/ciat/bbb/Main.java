package com.ciat.bbb;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.List;

import javax.imageio.ImageIO;

import org.lwjgl.BufferUtils;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.opengl.Texture;

import com.ciat.bbb.scenes.*;
import com.osreboot.ridhvl.HvlCoord;
import com.osreboot.ridhvl.action.HvlAction0;
import com.osreboot.ridhvl.display.collection.HvlDisplayModeDefault;
import com.osreboot.ridhvl.painter.HvlCamera;
import com.osreboot.ridhvl.painter.HvlGradient;
import com.osreboot.ridhvl.painter.HvlGradient.Style;
import com.osreboot.ridhvl.template.HvlTemplateInteg2D;

public class Main extends HvlTemplateInteg2D {

	private List<ByteBuffer> frames;

	public static boolean isFilming;
	public static int frameCount;

	public static Texture background;

	public static TrueTypeFont sansSerif16, segoeUI16, segoeUI12;

	private Scene currentScene;

	public static HvlCamera camera;

	public Main() {
		super(60, 1920, 1080, "BBB Video", new HvlDisplayModeDefault());
	}

	public static void main(String[] args) {
		new Main();
	}

	@Override
	public void initialize() {
		camera = new HvlCamera(0, 0, 0, 1, new HvlCoord(0, 0));

		isFilming = true;
		frames = new LinkedList<>();

		background = new HvlGradient(Style.RADIAL).addStop(0.0f, new Color(225, 236, 251)).addStop(1.0f, new Color(205, 226, 251)).toTexture(Display.getWidth(), Display.getDisplayMode().getHeight(), Display.getWidth() / 2, Display.getDisplayMode().getHeight() / 2, 0, Display.getDisplayMode().getHeight() / 2);

		try {
			// InputStream inputStream =
			// ResourceLoader.getResourceAsStream("res/Helvetica.ttf");

			// Font awtFont = Font.createFont(Font.TRUETYPE_FONT, inputStream);
			Font awtFont = new Font(Font.SANS_SERIF, Font.PLAIN, 16);
			// awtFont = awtFont.deriveFont(50f); // set font size
			sansSerif16 = new TrueTypeFont(awtFont, true);

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			// InputStream inputStream =
			// ResourceLoader.getResourceAsStream("res/Helvetica.ttf");

			// Font awtFont = Font.createFont(Font.TRUETYPE_FONT, inputStream);
			Font awtFont = new Font("Segoe UI", Font.PLAIN, 16);
			// awtFont = awtFont.deriveFont(50f); // set font size
			segoeUI16 = new TrueTypeFont(awtFont, true);

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			// InputStream inputStream =
			// ResourceLoader.getResourceAsStream("res/Helvetica.ttf");

			// Font awtFont = Font.createFont(Font.TRUETYPE_FONT, inputStream);
			Font awtFont = new Font("Segoe UI", Font.PLAIN, 12);
			// awtFont = awtFont.deriveFont(50f); // set font size
			segoeUI12 = new TrueTypeFont(awtFont, true);

		} catch (Exception e) {
			e.printStackTrace();
		}

		getTextureLoader().loadResource("BBBLogo");
		getTextureLoader().loadResource("FacebookFeed");
		getTextureLoader().loadResource("NoStamp");
		getTextureLoader().loadResource("RealDealText");
		getTextureLoader().loadResource("RotatingTextBlocker");
		getTextureLoader().loadResource("StarParticle");
		getTextureLoader().loadResource("CircuitBoard");
		getTextureLoader().loadResource("ConArtists");
		getTextureLoader().loadResource("Scammers");
		getTextureLoader().loadResource("GenerallyLessGoodIntentedPeople");
		getTextureLoader().loadResource("OnlineEducation");
		getTextureLoader().loadResource("SurveyScam");
		getTextureLoader().loadResource("CreditCardChip");
		getTextureLoader().loadResource("FBGiftExchange");
		getTextureLoader().loadResource("HolidayJob");
		getTextureLoader().loadResource("InvestorFraud");
		getTextureLoader().loadResource("Phishers");
		getTextureLoader().loadResource("SpamFilter");
		getTextureLoader().loadResource("BBBScamAlertsPage");
		getTextureLoader().loadResource("NoResultsFound");
		getTextureLoader().loadResource("StillNoStamp");
		getTextureLoader().loadResource("InternetMarketplace");
		getTextureLoader().loadResource("Rawr");
		getTextureLoader().loadResource("Highlight");
		getTextureLoader().loadResource("BlankChromeWindow");
		getTextureLoader().loadResource("RayBanPage");
		getTextureLoader().loadResource("FakeRayBanPage");
		getTextureLoader().loadResource("ObscureBank");
		getTextureLoader().loadResource("CheckTheWebsite");
		getTextureLoader().loadResource("HighQualityCheap");
		getTextureLoader().loadResource("StatesList");
		getTextureLoader().loadResource("BBBPhone");
		getTextureLoader().loadResource("BBBWebsite");

		getSoundLoader().loadResource("Intro");
		getSoundLoader().loadResource("PatheticScams");
		getSoundLoader().loadResource("ScamListAndSearch");
		getSoundLoader().loadResource("RagingInternetMarketplace");
		getSoundLoader().loadResource("TheDetailsCount");
		getSoundLoader().loadResource("Keyboard");
		getSoundLoader().loadResource("WholeTrack");

		setCurrentScene(new RayBanPostScene(this));
//		 setCurrentScene(new NewTechScene(this));
//		 setCurrentScene(new AllTheScamsScene(this));
//		 setCurrentScene(new ScamsListScene(this));
//		 setCurrentScene(new InternetRawrScene(this));
//		 setCurrentScene(new AddressMessingsScene(this));
//		 setCurrentScene(new InconsistenciesScene(this));
//		 setCurrentScene(new ContactScene(this));
	}

	@Override
	public void update(final float delta) {
		if (currentScene != null)
			currentScene.update(1f / 60);

		// HvlPainter2D.hvlDrawQuad(0, 0, Display.getWidth(),
		// Display.getDisplayMode().getHeight(), background);

//		camera.setAlignment(new HvlCoord(-(Display.getWidth() / 2) * (0.711458333f / 2), -(Display.getDisplayMode().getHeight() / 2) * 0.711458333f));
//		camera.setZoom(0.711458333f);
		camera.doTransform(new HvlAction0() {

			@Override
			public void run() {
				if (currentScene != null)
					currentScene.draw(1f / 60);
			}
		});
		
		if (isFilming) saveScreenshot(getScreenshot(), String.format("frame_%07d.png", frameCount++));

		if (currentScene == null) System.exit(0);
		
//		if (isFilming && frameCount++ >= 0) {
//			frames.add(getScreenshot());
//		}
//		
//		if (currentScene == null && isFilming) {
//			isFilming = false;
//			saveFrames();
//		}
	}

	public static ByteBuffer getScreenshot() {
		GL11.glReadBuffer(GL11.GL_FRONT);
		int width = Display.getWidth();
		int height = Display.getDisplayMode().getHeight();
		int bpp = 4; // Assuming a 32-bit display with a byte each for red,
						// green, blue, and alpha.
		ByteBuffer buffer = BufferUtils.createByteBuffer(width * height * bpp);
		GL11.glReadPixels(0, 0, width, height, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, buffer);

		return buffer;
	}

	public static void saveScreenshot(ByteBuffer bytes, String path) {
		System.out.println("Saving " + path);
		File file = new File("screen/" + path); // The file to save to.
		String format = "PNG"; // Example: "PNG" or "JPG"
		BufferedImage image = new BufferedImage(Display.getWidth(), Display.getDisplayMode().getHeight(), BufferedImage.TYPE_INT_RGB);
		Graphics2D gr = image.createGraphics();
		
		gr.setColor(java.awt.Color.red);
		gr.fillRect(0, 0, Display.getWidth(), Display.getDisplayMode().getHeight());
		
		for (int x = 0; x < Display.getWidth(); x++) {
			for (int y = 0; y < Display.getDisplayMode().getHeight(); y++) {
				int i = (x + (Display.getWidth() * y)) * 4;
				int r = bytes.get(i) & 0xFF;
				int g = bytes.get(i + 1) & 0xFF;
				int b = bytes.get(i + 2) & 0xFF;
//				image.setRGB(x, Display.getDisplayMode().getHeight() - (y + 1), (0xFF << 24) | (r << 16) | (g << 8) | b);
				gr.setColor(new java.awt.Color(r, g, b));
				gr.fillRect(x, Display.getDisplayMode().getHeight() - (y + 1), 1, 1);
			}
		}

		try {
			ImageIO.write(image, format, file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void setCurrentScene(Scene s) {
		currentScene = s;
	}

	public void saveFrames() {
		for (int i = 0; i < frames.size(); i++) {
			System.out.println("Saving frame " + i);
			saveScreenshot(frames.get(i), String.format("frame_%04d.png", i));
		}
		System.exit(0);
	}
}
