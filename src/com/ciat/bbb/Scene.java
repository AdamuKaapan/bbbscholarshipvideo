package com.ciat.bbb;

public abstract class Scene {
	protected Main parent;
	
	public Scene(Main parent) {
		this.parent = parent;
	}
	
	public abstract void update(float delta);
	
	public abstract void draw(float delta);
}
